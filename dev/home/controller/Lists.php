<?php

namespace app\home\controller;

use app\home\model\Total;
use think\Controller;

class Lists extends Controller
{
    public function list()
    {
        $data = \app\home\model\Goods::where('status',0)->select(); //查询已启用的商品分类信息

        return view('Lists/list',compact('data'));
    }

    public function show() //商品列表展示
    {
        $data = \app\home\model\Lists::paginate(3);

        return view('Lists/show',compact('data'));

    }
    public function info(){ //商品加入购物车详情展示
        $data = input();
        $id = $data['id'];
        $dataOne = \app\home\model\Lists::where('id',$id)->find();
        $dataTow = Total::all();
        return view('Lists/info',compact('dataOne','dataTow'));

    }
    public function collect() //收藏
    {
        if (!session('?uid')){ //判断是否登录
            $this->error('未登录不能收藏');die;
        }
        $data = input();
        $id = $data['id'];
        $where = [
          'status'=>$data['status'] == 0?1:0
        ];
        $ups = \app\home\model\Lists::where('id',$id)->update($where); //根据id修改收藏状态
        if ($ups){
            return redirect('/show');
        }
    }
    public function col(){ //展示已收藏的商品信息
        $data = \app\home\model\Lists::where('status',1)->select();

        return view('Lists/col',compact('data'));
    }



    public function cartlists()
    {
        if (!session('?uid')){
            $this->error('未登录不能添加到购物车');die;
        }
        $data = input();
        $id = $data['id'];

        $datas = \app\home\model\Lists::where('id',$id)->find();
        $addData = \app\home\model\Lists::where('id',$id)->setInc('number');
        return view('Lists/cartlists',compact('datas'));
    }
    public function jia()
    {
        $data = input();
        $id = $data['id'];
        $addData = \app\home\model\Lists::where('id',$id)->setInc('number',2);
        if ($addData){
            return redirect('/cartlists?id='.$id);
        }
    }
    public function jian()
    {
        $data = input();
        $id = $data['id'];
        $addData = \app\home\model\Lists::where('id',$id)->setDec('number',2);
        if ($addData){
            return redirect('/cartlists?id='.$id);
        }
    }

    public function tui(){
        session(null);

        return redirect('/list');
    }



}
