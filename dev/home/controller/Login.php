<?php

namespace app\home\controller;

use app\home\model\User;
use think\Controller;

class Login extends Controller
{
    public function selSgin()
    {
        return view('Login/selSgin'); //注册展示页面
    }
    public function sginDo()  //处理注册
    {
        $data = input(); //接值
        $addSgin = User::create($data,true);
        if ($addSgin){
            $this->success('注册成功','/selgin');
        }else{
            $this->error('注册失败');
        }

    }
    public function selgin() //登录展示页面
    {
        return view('Login/selgin');
    }
    public function logindo() //处理登录
    {
        $data = input(); //接值
        $where = [
          'username'=>$data['username'],
          'password'=>$data['password']
        ];

        $LoginRes = User::where($where)->find();

        if ($LoginRes){
            session('uid',$LoginRes->id);
            session('uname',$LoginRes->username);
            $this->success('登录成功','/list');
        }else{
            $this->error('登录失败');
        }






    }
}
